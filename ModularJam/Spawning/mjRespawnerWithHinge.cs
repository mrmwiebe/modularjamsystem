﻿using UnityEngine;
using System.Collections;

public class RespawnerWithHinge : MonoBehaviour {

	public GameObject spawnPosition;

	public GameObject objectPrefab;
	private GameObject myObject;

	private bool respawning = false;
	public float respawnDelay = 2;
	public bool spawnImmediatelyOnStart = true;

	void OnStart()
	{
		if (spawnImmediatelyOnStart) Respawn();
	}

	// Update is called once per frame
	void Update () {
		if(myObject == null && !respawning) StartCoroutine(Respawn());
	}

	IEnumerator Respawn()
	{
		respawning = true;

		yield return new WaitForSeconds(respawnDelay);

		myObject = GameObject.Instantiate(objectPrefab, spawnPosition.transform.position, spawnPosition.transform.rotation) as GameObject;
		myObject.GetComponent<HingeJoint>().connectedBody = GetComponent<Rigidbody>();
		myObject.transform.parent = transform;

		respawning = false;
	}
}
