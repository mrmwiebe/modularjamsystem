﻿
/*
	RESPAWNER
	---------

	Respawns a GameObject upon it's destruction.

	AttachHinge requires a Rigidbody on the spawner.

*/

using UnityEngine;
using System.Collections;

public class Respawner : MonoBehaviour {

	public GameObject spawnPosition;

	public GameObject objectPrefab;
	public GameObject particlePrefab;
	private GameObject myObject;

	public bool attachHinge = false;

	private bool respawning = false;

	//Spawn in random time
	public float respawnDelay = 2;
	public float respawnMaxDelay = 2;

	public bool spawnImmediatelyOnStart = true;

	void Start()
	{
		if (spawnImmediatelyOnStart) StartCoroutine(Respawn(0));
	}

	// Update is called once per frame
	void Update () {
		if(myObject == null && !respawning) StartCoroutine(Respawn(Random.Range(respawnDelay, respawnMaxDelay)));
	}

	IEnumerator Respawn(float time)
	{
		//Debug.Log (gameObject.name + " is respawning in " + time + " seconds.");
		respawning = true;

		yield return new WaitForSeconds(time);

		myObject = GameObject.Instantiate(objectPrefab, spawnPosition.transform.position, spawnPosition.transform.rotation) as GameObject;
		var myParticle = GameObject.Instantiate (particlePrefab, transform.position, transform.rotation) as GameObject;

		if(attachHinge) myObject.GetComponent<HingeJoint>().connectedBody = GetComponent<Rigidbody>();
		myObject.transform.parent = transform;

		respawning = false;
	}
}