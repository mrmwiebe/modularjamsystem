﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnRandomOnDeath : MonoBehaviour {

	public List<GameObject> objectsToSpawn;

	public mjHealth health;

	// Use this for initialization
	void Awake () 
	{
		health = gameObject.GetComponent<mjHealth> ();
		
		if (health == null)
			Debug.LogError ("SpawnObjectRandomOnDeath in " + gameObject.name + " has no mjHealth component");
		else
			gameObject.GetComponent<mjHealth>().onDeath += OnDeath;
	}
	
	void OnDeath()
	{
		var random = Random.Range(0, objectsToSpawn.Count);

		GameObject.Instantiate(objectsToSpawn[random], transform.position, Quaternion.identity);
	}
}
