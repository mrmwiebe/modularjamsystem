﻿using UnityEngine;
using System.Collections;

public class SpawnOnDeath : MonoBehaviour {

	public GameObject objectToSpawn;

	public mjHealth health;

	// Use this for initialization
	void Awake () 
	{
		health = gameObject.GetComponent<mjHealth> ();

		if (health == null)
			Debug.LogError ("SpawnOnDeath in " + gameObject.name + " has no mjHealth component");
		else
			gameObject.GetComponent<mjHealth>().onDeath += OnDeath;
	}
	
	void OnDeath()
	{
		GameObject.Instantiate(objectToSpawn, transform.position, Quaternion.identity);
	}
}
