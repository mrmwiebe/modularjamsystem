//Needs to be a child of the object that has Weapon Manager in it.

using UnityEngine;
using System.Collections;
using InControl;

public class WeaponTrigger : MonoBehaviour {

	/*

	public WeaponManager myWeaponManager;
	public float input;

	//OneTime - must release trigger to re-trigger
	//Rapid - keeps triggering as the trigger is held
	//Continuous - relays the position of the trigger for fluid weapons
	public enum TriggerType
	{
		OneTime,
		Rapid,
		Continuous
	}
	public TriggerType thisTriggerType;

	public float deadZone;
	public bool triggerReleased = true;
	public bool initialTriggerReleased = false;

	public int ammo;
	public int maxAmmo;

	//FireDelay Time
	//OneTime - must release trigger, and wait for this amount of time
	//Rapid - amount of time between
	//Continuous - Ignored
	public float fireDelayTime = 0;
	private float fireDelayCounter = 0;
	
	public delegate void TriggerAction();
	public event TriggerAction OnTrigger;


	//Animation
	//If waitForAnimation - cannot fire/drop weapon until animation is complete
	public Animation weaponAnimation;
	public bool waitForAnimation = false;

	void OnEnable()
	{
		ammo = maxAmmo;
		triggerReleased = false;
		initialTriggerReleased = false;
	}

	// Update is called once per frame
	void FixedUpdate () 
	{
		//Fetches the myWeaponManager.inputDevice from the player from the WeaponManager
		input = myWeaponManager.player.triggerAxis;

		//TRIGGER PULLED
		if(input > deadZone)
		{
			switch(thisTriggerType)
			{
				case TriggerType.OneTime:
					if(triggerReleased && CheckAnimation()) Trigger();
				break;

				case TriggerType.Rapid:
					if(fireDelayCounter <= 0 && CheckAnimation()) {
						Trigger();
						ResetDelayTimer();
					}
				break;

				case TriggerType.Continuous:
					
				break;
			}

			triggerReleased = false;
		}

		//TRIGGER RELEASED
		else {
			initialTriggerReleased = true;
			triggerReleased = true;
		}

		//Increment the delay!
		if(fireDelayCounter > 0) fireDelayCounter--;
	}

	void ResetDelayTimer()
	{
		//Reset the delay counter only when trigger's been pulled
		if(fireDelayCounter <= 0) fireDelayCounter = fireDelayTime;
	}

	void DecreaseAmmo()
	{
		ammo -= 1;

		if (ammo <= 0) myWeaponManager.ResetItems();
	}

	//Just makes sure there's things attached to this trigger
	void Trigger()
	{
		if (OnTrigger != null) {
			OnTrigger();
			DecreaseAmmo();

			if(weaponAnimation != null) weaponAnimation.Play();
		}
		else Debug.Log("There's nothing attached to this trigger!!!");
	}

	bool CheckAnimation() 
	{
		if (waitForAnimation) 
		{
			if(!weaponAnimation.isPlaying) return true;

			return false;
		} 

		else return true;
	}

	*/
}
