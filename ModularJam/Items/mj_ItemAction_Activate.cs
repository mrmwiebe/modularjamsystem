﻿using UnityEngine;
using System.Collections;

public class ActivateToy : MonoBehaviour 
{
	public bool toggle = false;
	public bool isActivated = false;

	// Update is called once per frame
	void Update () 
	{
		if(toggle)
		{
			isActivated = !isActivated;

			if(isActivated) SendMessage("Activate");
			if(!isActivated) SendMessage("Deactivate");

			toggle = false;
		}
	}


}
