﻿using UnityEngine;
using System.Collections;

public class ProjectileCore : MonoBehaviour {

	public bool isMortal = true;
	public float life = 5f;
	
	//Lets make an event that other scripts on this object can reference
	public delegate void DestroyEvent();
	public event DestroyEvent OnDestroy;

	// Update is called once per frame
	void Update () {
		if(isMortal)
		{
			life -= Time.deltaTime * 1;
			if(life <= 0) DestroyMe();
		}
	}

	void DestroyMe()
	{
		if(OnDestroy != null) OnDestroy();
		Destroy(gameObject);
	}
}
