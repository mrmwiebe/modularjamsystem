﻿using UnityEngine;
using System.Collections;


public class mjHealth : MonoBehaviour {

	public delegate void _OnDeath();
	public event _OnDeath onDeath;

	public int health;

	private mjEntity entity;

	void Awake()
	{
		entity = GetComponent<mjEntity> ();

		if (entity == null) 
		{
			Debug.LogError ("mjHealth in " + gameObject.name + " has no mjEntity");
		}
		else 
		{
			//Make sure it calls the Entity's onDeath when out of health
			onDeath += entity.OnDeath;
		}
	}

	public void DoDamage(int amount)
	{
		health -= amount;
		if(health < 0) onDeath();
	}

}
