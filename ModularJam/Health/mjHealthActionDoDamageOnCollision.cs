﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class mjHealthActionDoDamageOnCollision : MonoBehaviour {

	public bool continuousDamage = false;
	public bool destroyOnHit = true;
	public bool destroyOnAnyHit = false;
	public int damageAmount = 1;

	public List<string> ignoreTags;
	public List<GameObject> ignoreObjects;

	void OnCollisionEnter(Collision c)
	{
		if(!ignoreTags.Contains(c.collider.tag) && !ignoreObjects.Contains(c.gameObject)) 
			DoDamage(c);
	}

	void OnCollisionStay(Collision c)
	{
		if(!ignoreTags.Contains(c.collider.tag) && !ignoreObjects.Contains(c.gameObject) && continuousDamage) DoDamage(c);
	}

	private void DoDamage(Collision c)
	{
		if (c.collider.GetComponent<mjHealth> () != null) {
			c.collider.GetComponent<mjHealth> ().DoDamage (damageAmount);
			DestroyMe();
		}

		if(destroyOnAnyHit) Destroy(gameObject);
	}

	private void DestroyMe()
	{
		if(destroyOnHit) Destroy(gameObject);
	}
}