﻿using UnityEngine;
using System.Collections;

public class mjRotateTowardsInputStick : MonoBehaviour {
	
	public mjMoveInputDualController moveInput;

	/*
	public bool smoothing = false;
	public float smoothTime = 1f;
	*/

	public enum InputToWatch {RStick, LStick};
	public InputToWatch inputToWatch;
	public Vector2 inputAxis;
	
	public enum AngleOutput {rollX};
	//public enum AngleOutput {rollX, rollY, rollZ};
	public AngleOutput angleOutput;

	public float deadZone = 0.1f;

	void Awake () {
		if (moveInput == null) {

			moveInput = GetComponent<mjMoveInputDualController> ();

			if (moveInput == null)
				Debug.LogError ("mjRotateTowardsInputStick in " + gameObject.name + " has no mjMoveInputDualController");

		}
	}

	// Update is called once per frame
	void FixedUpdate () {

		float newX = transform.localEulerAngles.x;
		float newY = transform.localEulerAngles.y;
		float newZ = transform.localEulerAngles.z;

		switch(inputToWatch)
		{
			case InputToWatch.LStick:
				inputAxis = moveInput.inputDevice.LeftStick;
			break;

			case InputToWatch.RStick:
				inputAxis = moveInput.inputDevice.RightStick;
			break;
		}

		switch(angleOutput)
		{
			case AngleOutput.rollX:
				newX = inputAxis.x;
				newZ = inputAxis.y;
				newY = 0;
			break;

			/*

			case AngleOutput.rollY:
				newX = inputAxis.x;
				newY = inputAxis.y;
			break;

			case AngleOutput.rollZ:
				newY = inputAxis.y;
				newZ = inputAxis.x;
			break;
			*/	
		}

		Vector3 newVector = new Vector3 (newX, newY, newZ);
		Quaternion newRotation = Quaternion.LookRotation(newVector);

		if(newVector.magnitude > deadZone) {
			//if(smoothing) transform.rotation = Quaternion.Lerp (from.rotation, to.rotation, Time.time * speed);
			transform.rotation = newRotation;
		}
	}
}
