﻿/*
 * 	Requires mjMoveInputDualController class to watch
 */

using UnityEngine;
using System.Collections;

public class mjRotateAxisByStickAxis : MonoBehaviour {
	
	public mjMoveInputDualController moveInput;
	
	public bool smoothing = false;
	public float smoothTime = 1f;

	//Should it be addive instead of absolute?
	/*
	public bool additive = false;
	*/
	public float amount = 0.1f;
	
	public enum InputToWatch {RStickX, RStickY, LStickX, LStickY};
	public InputToWatch inputToWatch;
	public float inputAxis;

	public enum AngleOutput {rollX, rollY, rollZ};
	public AngleOutput angleOutput;
	
	public float deadZone = 0.1f;
	
	void Awake () {
		if (moveInput == null) {
			
			moveInput = GetComponent<mjMoveInputDualController> ();
			
			if (moveInput == null)
				Debug.LogError ("mjRotateByInputStick in " + gameObject.name + " has no moveInput");
			
		}
	}
	
	// Update is called once per frame
	void Update () {
		switch(inputToWatch)
		{
			case InputToWatch.LStickX:
				inputAxis = moveInput.inputDevice.LeftStickX;
				break;

			case InputToWatch.LStickY:
				inputAxis = moveInput.inputDevice.LeftStickY;
				break;

			case InputToWatch.RStickX:
				inputAxis = moveInput.inputDevice.RightStickX;
				break;

			case InputToWatch.RStickY:
				inputAxis = moveInput.inputDevice.RightStickY;
				break;

		}

		if (inputAxis < deadZone && inputAxis > -deadZone)
			inputAxis = 0;

		float newX = transform.rotation.x;
		float newY = transform.rotation.y;
		float newZ = transform.rotation.z;

		switch (angleOutput) {
		case AngleOutput.rollX:
			newX = inputAxis * amount;
			break;

		case AngleOutput.rollY:
			newY = inputAxis * amount;
			break;

		case AngleOutput.rollZ:
			newZ = inputAxis * amount;
			break;
		}

		//if(smoothing) transform.rotation = Quaternion.Lerp (from.rotation, to.rotation, Time.time * speed);
		transform.eulerAngles = new Vector3 (newX, newY, newZ);
	}
}
