﻿using UnityEngine;
using System.Collections;

public class mjRotateTowardsRigidbodyVelocity : MonoBehaviour {

	public Rigidbody rigidbodyToWatch;
	public bool thisRigidbody;

	public bool smoothing = false;
	public float smoothTime = 1f;

	public bool lockX = true;
	public bool lockY = false;
	public bool lockZ = false;

	public float deadZone = 0.1f;

	void Awake () {
		if(thisRigidbody) rigidbodyToWatch = transform.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		if(rigidbodyToWatch.velocity.magnitude > deadZone) {
			float velocityX = rigidbodyToWatch.velocity.x;
			float velocityY = rigidbodyToWatch.velocity.y;
			float velocityZ = rigidbodyToWatch.velocity.z;

			if (lockX) velocityX = 0;
			if (lockY) velocityY = 0; 
			if (lockZ) velocityZ = 0;

			//if(smoothing) //rigidbodyToWatch.DORotate(new Vector3(velocityX, velocityY, velocityZ), smoothTime);
			transform.rotation = Quaternion.LookRotation( new Vector3(velocityX, velocityY, velocityZ));
		}
	}
}
