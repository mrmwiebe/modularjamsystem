﻿using UnityEngine;
using System.Collections;

public class mjMoveInput : MonoBehaviour {

	public Vector3 moveVector;
	public delegate void MoveAxes(Vector3 movement);
	public MoveAxes moveAxes;
	
	public delegate void SpinAxes(Vector3 rotation);
	public SpinAxes spinAxes;

	public delegate void Action1 ();
	public Action1 action1;

	public delegate void Action2 ();
	public Action2 action2;

	public delegate void Action3 ();
	public Action3 action3;

	public delegate void Action4 ();
	public Action4 action4;

	public delegate void Action5 ();
	public Action5 action5;

	public delegate void Action6 ();
	public Action6 action6;

	public delegate void Trigger1 ();
	public Trigger1 trigger1;

	public delegate void Trigger2 ();
	public Trigger2 trigger2;

}
