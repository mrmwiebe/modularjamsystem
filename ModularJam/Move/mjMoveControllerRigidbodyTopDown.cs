﻿using UnityEngine;
using System.Collections;
using InControl;

[RequireComponent (typeof (Rigidbody))]

public class mjMoveControllerRigidbodyTopDown: MonoBehaviour {

	public float gravity = 10.0f;
	public float maxVelocityChange = 10.0f;
	public bool canJump = true;
	public float jumpHeight = 2.0f;
	public bool grounded = false;
	public bool stunned = false;
	public float speedMultiplier = 1;

	private Vector3 targetVelocity = Vector3.zero;
	private mjMoveInput input;

	void Awake () {
		//Check if required components exist
		input = gameObject.GetComponent<mjMoveInput>();

		if (input == null) 
			Debug.LogError ("Move Controller Rigidbody: No Input Component found");

		GetComponent<Rigidbody>().freezeRotation = true;
		GetComponent<Rigidbody>().useGravity = false;

		input.moveAxes += SetTargetVelocity;
	}
	
	void FixedUpdate () {
		if (grounded && !stunned) {
			// Calculate how fast we should be moving

			targetVelocity = transform.TransformDirection(targetVelocity);
			
			// Apply a force that attempts to reach our target velocity
			Vector3 velocity = GetComponent<Rigidbody>().velocity;
			Vector3 velocityChange = (targetVelocity - velocity);
			velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
			velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
			velocityChange.y = 0;
			GetComponent<Rigidbody>().AddForce(velocityChange, ForceMode.VelocityChange);

			//Debug.Log ("Velocity Change: " + velocityChange);
			
			// Jump
			if (canJump && Input.GetButton("Jump")) {
				GetComponent<Rigidbody>().velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(), velocity.z);
			}
		}
		
		// We apply gravity manually for more tuning control
		GetComponent<Rigidbody>().AddForce(new Vector3 (0, -gravity * GetComponent<Rigidbody>().mass, 0));
		
		grounded = false;
	}
	
	void OnCollisionStay () {
		grounded = true;    
	}
	
	float CalculateJumpVerticalSpeed () {
		// From the jump height and gravity we deduce the upwards speed 
		// for the character to reach at the apex.
		return Mathf.Sqrt(2 * jumpHeight * gravity);
	}

	public void SetTargetVelocity (Vector3 move) {
		targetVelocity = move * speedMultiplier;
	}
}
