﻿using UnityEngine;
using System.Collections;

public class mjRotateTowardsInputVelocity : MonoBehaviour {

	public GameObject objectToWatch;
	public mjMoveInput moveInput;

	public bool smoothing = false;
	public float smoothTime = 1f;

	public bool lockX = true;
	public bool lockY = false;
	public bool lockZ = false;

	public float deadZone = 0.1f;

	void Awake () {
		if (objectToWatch == null) {

			moveInput = GetComponent<mjMoveInput> ();

			if (moveInput == null)
				Debug.LogError ("RotateTowardsInputVelocity in " + gameObject.name + " has no moveInput");

		} else {

			moveInput = objectToWatch.GetComponent<mjMoveInput>();

			if(moveInput == null)
				Debug.LogError ("RotateTowardsInputVelocity in " + gameObject.name + " has no moveInput in watched object " + objectToWatch.name);
		}
	}

	// Update is called once per frame
	void Update () {
		if(moveInput.moveVector.magnitude > deadZone) {
			float velocityX = moveInput.moveVector.x;
			float velocityY = moveInput.moveVector.y;
			float velocityZ = moveInput.moveVector.z;

			if (lockX) velocityX = 0;
			if (lockY) velocityY = 0; 
			if (lockZ) velocityZ = 0;

			//if(smoothing) transform.rotation = Quaternion.Lerp (from.rotation, to.rotation, Time.time * speed);
			transform.rotation = Quaternion.LookRotation( new Vector3(velocityX, velocityY, velocityZ));
		}
	}
}
