﻿using UnityEngine;
using System.Collections;
using InControl;
using UnityEngine.UI;

[RequireComponent (typeof (mjEntityPlayer))]

public class mjMoveInputDualController : mjMoveInput {

	//Call the inheritance
	void mjMoveInput() {}

	public InputDevice inputDevice;

	private mjEntityPlayer player;

	public string debugtext;

	// Use this for initialization
	void Awake () {
		player = GetComponent<mjEntityPlayer> ();
	}

	void FixedUpdate()
	{
		inputDevice = (InputManager.Devices.Count > player.number) ? InputManager.Devices[player.number] : null;
		//inputDevice = InputManager.ActiveDevice;
		
		if (inputDevice == null)
		{
			//no controller exists
			Debug.Log ("No Controller" + InputManager.Devices);
		}
		else
		{
			moveVector = new Vector3(inputDevice.LeftStickX, 0, inputDevice.LeftStickY);
			moveAxes(moveVector);
		}

	}
}
