﻿using UnityEngine;
using System.Collections;

public class DrawGizmo : MonoBehaviour {

	public enum GizmoType
	{
		WireCube,
		Icon,
		Circle
	}
	public GizmoType thisGizmo;

	public string icon;

	public float radius;

	public Color color;

	void Awake()
	{
		//Destroy (this);
	}

	void OnDrawGizmos () 
	{
		Gizmos.color = color;

		switch(thisGizmo)
		{
			case GizmoType.Icon:
				Gizmos.DrawIcon (transform.position, icon, true);
			break;

			case GizmoType.Circle:
				Gizmos.DrawWireSphere (transform.position, radius);
			break;

			case GizmoType.WireCube:
				// Draw a yellow cube at the transforms position
				Gizmos.DrawWireCube (transform.position, new Vector3 (1,1,1));
			break;
		}

	}
}
