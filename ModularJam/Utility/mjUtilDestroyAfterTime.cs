﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTime : MonoBehaviour {

	public float time = 5;

	// Use this for initialization
	void Start () {
		StartCoroutine (DestroyMe());
	}

	private IEnumerator DestroyMe()
	{
		yield return new WaitForSeconds(time);

		Destroy(gameObject);
	}
}
