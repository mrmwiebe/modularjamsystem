﻿using UnityEngine;
using System.Collections;

public class HideShowOnActivate : MonoBehaviour 
{

	void IsActivated()
	{
		GetComponent<Renderer>().enabled = true;
	}

	void IsDeactivated()
	{
		GetComponent<Renderer>().enabled = false;
	}
}
