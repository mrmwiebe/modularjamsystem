﻿using UnityEngine;
using System.Collections;

public class SineBobbing : MonoBehaviour {

	public float seconds = 1;

	public float offset = 0;

	public Vector3 positionMove;
	private Vector3 originalPosition;

	public bool constantSpin = false;
	public Vector3 rotationMove;


	// Use this for initialization
	void Start () {
		originalPosition = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
		var sine = Mathf.Sin(Time.time / seconds + offset);

		Vector3 pos = originalPosition;

		pos.y += positionMove.y * sine;
		pos.x += positionMove.x * sine;
		pos.z += positionMove.z * sine;

		transform.localPosition = pos;


		Vector3 rot = transform.localEulerAngles;

		rot.y += rotationMove.y;
		rot.x += rotationMove.x;
		rot.z += rotationMove.z;

		transform.localEulerAngles = rot;
	}
}
