﻿using UnityEngine;
using System.Collections;

public class Shake : MonoBehaviour {

	private Transform myParent;

	public float amount = 0.1f;
	public float decay = 0.001f;

	void Start()
	{
		myParent = transform.parent.transform;
	}

	// Update is called once per frame
	void Update () 
	{	
		if(amount > 0.01)
		{
			Debug.Log ("Shake it: " + amount);

			amount *= decay;

			var pt = myParent.localPosition;
			transform.localPosition = pt + new Vector3(pt.x + Random.Range(-amount, amount), pt.y + Random.Range(-amount, amount), pt.z);
		}
	}
}
